//
//  APICall.swift
//  MvvmForTableView
//
//  Created by ADMIN on 11/02/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

protocol StudentViewModelDelegate {
    func didReloadTableView()
}
