//
//  ViewModel.swift
//  MvvmForTableView
//
//  Created by ADMIN on 11/02/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

import Foundation

class StudentViewModel {
    var studentViewModelDelegate: StudentViewModelDelegate?
    var nameList = [String]()
    
    var getStudentCredentialModel: GetStudentCredentialModel
    
    init(getStudentCredentialModel: GetStudentCredentialModel) {
        self.getStudentCredentialModel = getStudentCredentialModel
    }
    
    func fetchFromServer() {
        GetMethodAPICall.sharedInstance.getData(url: getStudentCredentialModel.url, header: getStudentCredentialModel.header){ response in
            let studentData = try? JSONDecoder().decode(StudentDataModel.self, from: response)
            
            for name in studentData!.studentdetails{
                self.nameList.append(name.studentname)
            }
//            self.studentViewModelDelegate?.didReceiveStudentName(self.nameList)
            DispatchQueue.main.async {
                self.studentViewModelDelegate?.didReloadTableView()
            }
        }
    }
}
