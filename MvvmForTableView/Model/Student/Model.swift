//
//  Model.swift
//  MvvmForTableView
//
//  Created by ADMIN on 11/02/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

import Foundation

struct GetStudentCredentialModel {
    var url: String
    var header: [String: String]?
}

// MARK: - StudentData
struct StudentDataModel: Codable {
    var studentdetails: [Studentdetail]
}

// MARK: - Studentdetail
struct Studentdetail: Codable {
    var school, studentname, studentdetailClass, board: String
    
    enum CodingKeys: String, CodingKey {
        case school, studentname
        case studentdetailClass = "class"
        case board
    }
}
