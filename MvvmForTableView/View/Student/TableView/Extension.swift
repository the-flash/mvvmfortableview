//
//  TableViewExtension.swift
//  MvvmForTableView
//
//  Created by ADMIN on 12/06/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource, StudentViewModelDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentViewModel.nameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell", for: indexPath) as! StudentTableViewCell
        cell.NameLbl.text = studentViewModel.nameList[indexPath.row]
        return cell
    }
    
    func didReloadTableView() {
        self.tableView.reloadData()
    }
}
