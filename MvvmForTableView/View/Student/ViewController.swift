//
//  ViewController.swift
//  MvvmForTableView
//
//  Created by ADMIN on 11/02/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var studentViewModel: StudentViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let getStudentCredentialModel = GetStudentCredentialModel(url: LocalConstant.sharedInstance.BaseURL, header: LocalConstant.sharedInstance.Header)
        
        studentViewModel = StudentViewModel(getStudentCredentialModel: getStudentCredentialModel)
        studentViewModel.studentViewModelDelegate = self
        studentViewModel.fetchFromServer()
    }
}

